= Making a Fast USB ASP

== Introduction

These instructions have been prepared whilst using Ubuntu 20.04. Of
course, these instructions (or variations of them) them may work on
other versions of Ubuntu or other flavours of Linux, but such cases have
not been tested.

== Tools

* ST-LINK V3
* USB Type A to Micro-B cable
* ST32 `Blue Pill' or `Black Pill' fitted with an STM32F103C6T6 (this
will be referred to as the Blue Pill).
* 4 x female-to-female jumper wire
* 2 x male-to-female jumper wire
* 3.3V power source
* A means of connecting the 3.3V power source to the male end of the 2
jumper wires

== Hardware Preparation

[arabic]
. Start with everything disconnected.
. Connect the Blue Pill to the ST-LINK V3 using the four
female-to-female jumper wires in accordance with the following table.
Note that I have seen variants of the Blue Pill with the 4-pin connector
numbered in the opposite way to other variants - however the relative
physical position of the signals was the same. Therefore, to prevents
accidents I have omitted the pin numbers from the following table and
used only the signal names:
+
[cols=",,",options="header",]
|===
|Blue Pill 4-pin Header |ST-LINKv3 CN6 |Wire colour in photo
|3V3 |1 (T_VCC) |Purple
|DIO |4 (DIO) |Grey
|CLK |2 (CLK) |White
|GND |3 (GND) |Black
|===
. Connect a powered-off 3.3V power source to the ST-LINK V3, using the
two male-to-female jumper wires in accordance with the following table:
+
[cols=",,",options="header",]
|===
|ST-LINKv3 CN2 |Power Supply |Wire colour in photo
|1 (T_VCC) |+3.3V |Yellow
|4 (GND) |0V |Green
|===
. Ensure the only connections are from the Blue Pill to the ST-LINK V3,
and from the ST-LINK V3 to the power supply. There should be no
connection between the Blue Pill and your computer. There is no need to
switch on the 3.3V power source yet.
. Connect the ST-LINK V3 to the Ubuntu machine using the USB cable.

== Software Utility Preparation

=== Installing stlink-tools

Note that version 1.7.0 (or later) of stlink-tools is required in order
to work with the ST-LINK V3.

[arabic]
. Check if stlink-tools v1.7.0-26 (or newer) is already installed:
+
[source,bash]
----
st-info --version
----
. If stlink-tools v1.7.0-26 (or newer) is already installed, jump down
to the `Installing gcc-arm-none-eabi in Ubuntu 20.04` section.
. Remove any pre-existing version of stlink-tools:
+
[source,bash]
----
sudo apt remove -y stlink-tools
----
. Install the open source STM32 MCU programming toolset (stlink, which
originates here: https://github.com/stlink-org/stlink) on a Ubuntu 20.04
machine, as follows:
+
[source,bash]
----
sudo apt-get install -y gcc build-essential cmake libusb-1.0 libusb-1.0-0-dev libgtk-3-dev pandoc
cd ~
rm -rf ./stlink
git clone https://github.com/stlink-org/stlink.git
cd stlink
make clean
make release
sudo make install
sudo cp -a config/udev/rules.d/* /etc/udev/rules.d/
sudo udevadm control --reload-rules
sudo udevadm trigger
----
. Confirm that stlink-tools v1.7.0-26 (or newer) is now installed:
+
[source,bash]
----
st-info --version
----

=== Installing `gcc-arm-none-eabi` in Ubuntu 20.04

[arabic]
. Check whether `gcc-arm-none-eabi` is already installed:
+
[source,bash]
----
export PATH="/opt/gcc-arm/bin:$PATH"
arm-none-eabi-gcc --version
----
. If `gcc-arm-none-eabi` is not installed:
[loweralpha]
.. install it as follows:
+
[source,bash]
----
mkdir -p /root/downloads
cd /root/downloads
sudo apt install -y curl
curl -L --output "gcc-arm-none-eabi.tar.bz2" https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2020q2/gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2
sudo -i
mkdir -p /opt
cd /opt
tar xjf /root/downloads/gcc-arm-none-eabi.tar.bz2
mv gcc-arm-none-eabi-9-2020-q2-update gcc-arm
exit
export PATH="/opt/gcc-arm/bin:$PATH"
----
[loweralpha, start=2]
.. Test that `gcc-arm-none-eabi` is installed properly:
+
[source,bash]
----
arm-none-eabi-gcc --version
----

== Programming the Blue Pill with the FASTUSBasp Firmware

[arabic]
. If you have a Blue Pill rather than a Black Pill, ensure it has had
R10 changed from 10k to 1k5 (in some cases the Blue Pill will have
shipped with the correct value for R10).
. Ensure the Blue Pill jumper closest to the reset button (or labelled
as B1 on the Black Pill) is in position `1' (or `+') and the Blue Pill
jumper furthest from the reset button (or labelled as B0 on the Black
Pill) is in position `0' (or `-').
. Power-up/enable the 3.3V power source which is connected to the
ST-LINK V3 and the Blue Pill's Red power LED should illuminate.
. Within the Ubuntu 20.04 machine, download the binary file for flashing
the Blue Pill:
+
[source,bash]
----
mkdir -p /root/downloads
cd /root/downloads
sudo apt install -y curl
curl -L --output "fastusbasp.bin" https://gitlab.com/sean-burns-public/fast-usb-asp/-/jobs/1322246196/artifacts/raw/fastusbasp-v1.0.0-12-gb0e0881.bin
curl -L --output "fastusbasp1.bin" https://gitlab.com/sean-burns-public/fast-usb-asp/-/jobs/1418741000/artifacts/raw/fastusbasp-v1.0.0-14-g7516867.bin
curl -L --output "fastusbasp2.bin" https://gitlab.com/sean-burns-public/fast-usb-asp/-/jobs/1418755971/artifacts/raw/fastusbasp-v1.0.0-14-g53202be.bin
curl -L --output "fastusbasp3.bin" https://gitlab.com/sean-burns-public/fast-usb-asp/-/jobs/1418759233/artifacts/raw/fastusbasp-v1.0.0-14-gf96670e.bin
----
. Enter the following commands to program the FASTUSBasp firmware into
the Blue Pill (note that depending what firmware you already have in the
Blue Pill, you may need to hold the reset button whilst executing the
erase command):
+
[source,bash]
----
st-info --probe
st-flash --connect-under-reset erase
----
+
Now, any of the following final commands to make any of 4 uniquely
identifiable instances of the fastusbasp:
+
[source,bash]
----
st-flash write /root/downloads/fastusbasp.bin 0x8000000
st-flash write /root/downloads/fastusbasp1.bin 0x8000000
st-flash write /root/downloads/fastusbasp2.bin 0x8000000
st-flash write /root/downloads/fastusbasp3.bin 0x8000000
----
. Switch off the 3.3V power supply.
. Disconnect the jumper wires from the 4-way header of the Blue Pill.

== Configuring Ubuntu to Use the FastUSBasp

[arabic]
. Install the appropriate udev rules:
+
[source,bash]
----
su
cat >> /etc/udev/rules.d/fastusbasp.rules << EOF
SUBSYSTEMS=="usb", ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="05dc", MODE="0666"
EOF
/etc/init.d/udev restart
----
. Install avrdude:
+
[source,bash]
----
apt install -y avrdude
----
. If you want to use multiple `fastusbasp` instances from one computer,
insert the following block of text into the `/etc/avrdude.conf` file:
+
[source,text]
----
programmer
id    = "usbasp-clone-1";
desc  = "Any usbasp clone with correct VID/PID";
type  = "usbasp";
connection_type = usb;
usbvid    = 0x16C0; # VOTI
usbpid    = 0x05DC; # Obdev's free shared PID
#usbvendor  = "";
usbproduct = "fastusbasp_instance_1";
;

programmer
id    = "usbasp-clone-2";
desc  = "Any usbasp clone with correct VID/PID";
type  = "usbasp";
connection_type = usb;
usbvid    = 0x16C0; # VOTI
usbpid    = 0x05DC; # Obdev's free shared PID
#usbvendor  = "";
usbproduct = "fastusbasp_instance_2";
;

programmer
id    = "usbasp-clone-3";
desc  = "Any usbasp clone with correct VID/PID";
type  = "usbasp";
connection_type = usb;
usbvid    = 0x16C0; # VOTI
usbpid    = 0x05DC; # Obdev's free shared PID
#usbvendor  = "";
usbproduct = "fastusbasp_instance_3";
;
----

== Using the Blue Pill as a FastUSBasp

[arabic]
. Ensure both of the jumpers on the Blue Pill are in position `0' (or
`-'), so that the Blue Pill's integrated UART bootloader isn't enabled.
. Connect the USB cable from a Ubuntu machine to the Blue Pill.
. Connect the Blue Pill (acting as the FastUSBasp) to the target
device/PCBA in accordance with the following table:
+
[width="100%",cols="18%,20%,11%,35%,16%",options="header",]
|===
|Blue Pill IO |Blue Pill Pin |AVR IO |ISP Signal |UART Signal
|+3V3 Output |Pin 1 of 4
**WARNING:** The +3V3 output of the Blue Pill must only be connected to the target device if it doesn't have its own power supply.[1]
| |+3V3 Supply (Optional
**WARNING:** The +3V3 output of the Blue Pill must only be connected to the target device if it doesn't have its own power supply.[1])
|

|GND |Pin 4 of 4 |GND |GND |GND

|PA8 |Labelled as A8 |RST |RST |

|PB13 |Labelled as B13 |SCK |SCK |

|PB14 |Labelled as B14 |MISO |MISO |

|PB15 |Labelled as B15 |MOSI |MOSI |

|PA9 |Labelled as A9 |RX | |Tx [2]

|PA10 |Labelled as A10 |TX | |Rx [2]
|===
+
Notes:
+
[[myfootnote1]]*^1^* WARNING: The +3V3 output of the Blue Pill must only be connected to the target device if the target device doesn't have its own power supply. +
[[myfootnote2]]*^2^* Tx/Rx is written from the perspective of the Blue Pill (i.e. Tx is an output
of the Blue Pill).

== Example Operations with the Atmel AtMega1284P as a Target

[arabic]
. If not done already, disconnect the FastUSBasp from the Ubuntu machine
so that it is powered off.
. Prepare a target PCBA but ensure it is powered off.
. Connect the FastUSBasp to a target ATmega1284 (PCBA), as per the
following table:
+
[width="100%",cols="20%,23%,23%,34%",options="header",]
|===
|ISP Signal |Pin of Blue Pill |Connection Medium |Atmel ISP Connector
|MISO |B14 |Wire / direct |Pin 1 of 6
|SCK |B13 |Wire / direct |Pin 3 of 6
|MOSI |B15 |Wire / direct |Pin 4 of 6
|RST |A8 |Wire / direct |Pin 5 of 6
|GND |GND |Wire / direct |Pin 6 of 6
|===
. Apply power to the target ATmega1284 (PCBA).
. Ensure both of the jumpers on the Blue Pill are in position `0' (or
`-'), so that the Blue Pill's integrated UART bootloader isn't enabled.
. Connect the USB cable from a Ubuntu machine to the FastUSBasp (the
power LED should illuminate).
+
Note: /dev/ttyACM0 is the extra/spare serial port.
. Probe the target ATmega1284 device (choose the correct command
depending on which usbasp-clone instance you are using):
+
[source,bash]
----
avrdude -c usbasp-clone -p m1284
avrdude -c usbasp-clone-1 -p m1284
avrdude -c usbasp-clone-2 -p m1284
avrdude -c usbasp-clone-3 -p m1284
----
+
[arabic]
. Read the flash memory of the target ATmega1284 device into a file
named `flash.bin' (replace usbasp-clone with the correct instance for
your needs):
+
[source,bash]
----
avrdude -c usbasp-clone -p m1284 -U flash:r:flash.bin:r
----
+
[arabic]
. Write to the flash memory of the target ATmega1284 device from a file
named `my_hex_file.hex':
+
[source,bash]
----
avrdude -c usbasp-clone -p m1284 -U flash:w:my_hex_file.hex
----
+
Example:
+
[source,bash]
----
cd <root-of-repository>
avrdude -c usbasp-clone -p m1284 -U flash:w:./build/bin/my_hex_file.hex
----
